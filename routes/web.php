<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['auth'])->group(function () {

Route::get('/uploadimg', 'HomeController@uploadimg');
Route::get('/listdata', 'HomeController@listdata');
Route::post('/uploadimgpost', 'HomeController@uploadimgpost');
Route::get('/editimage/{id}', 'HomeController@editimage');
Route::get('/deleteimage/{id}', 'HomeController@deleteimage');
Route::post('/updateimgpost/{id}', 'HomeController@updateimgpost');

});