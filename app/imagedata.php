<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class imagedata extends Model
{
    //
    // use Notifiable;
    use SoftDeletes;
    public $table = 'imagedata';
    public $primaryKey = 'id';
    public $fillable = [
        'image',
        'target_id',
        'video',
        'txt',
        'obj',
        'ref_img',
        
    ];

}
