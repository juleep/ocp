<?php

namespace App\Http\Controllers;

use App\imagedata;
use DateTime;
use Request;
use Session;

const JSON_CONTENT_TYPE = 'application/json';
const ACCESS_KEY = 'ad3e55b51f4dbff73c0afd54560b4b183e8a948d';
const SECRET_KEY = '3a548746498b55f709108b8ff68d65295dfc9206';
const BASE_URL = 'https://vws.vuforia.com';
const TARGETS_PATH = '/targets';
    // public $imagePath = '/home/admin-pc/Desktop/';
    // public $imageName = 'oreo_marker.jpeg';
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // print_r(json_encode(json_decode('data added successfully target : 12345654545')));die;
        return view('home');
    }

    public function uploadimg()
    {
        return view('uploadimg');
    }
    public function uploadimgpost(Request $request)
    {
        $data = Request::all();
        $image = Request::file('mainimg');
        if($image->getClientSize() >= 2000000){
            Session::flash('error','Please upload file size less then 2M');
            return redirect('uploadimg');
        }else{
            $input = $image->getClientOriginalName();
            $fullpath = '/Images/Uploads/Main_Image/'.$input;
            $exist = imagedata::where('image',$fullpath)->get();
            if(count($exist) != 0){
                Session::flash('error','image is exist');
                return redirect('uploadimg');
            }
            $destinationPath = base_path('storage/app/public/Images/Uploads/Main_Image');
            // print_r($destinationPath);die;
            $image->move($destinationPath, $input);
            $resp = $this->addTarget(base_path('storage/app/public/Images/Uploads/Main_Image/'),$input);
            $create = ['image'=>$fullpath,'target_id'=>$resp];

            imagedata::create($create);
            // print_r(json_encode(json_decode($resp,true)));die;
            Session::flash('success','record created successfully !');
            return redirect('/listdata');    
        }
    }
    public function listdata()
    {
        $data = array();
        $data = imagedata::get();
        return view('listdata')->with('data',$data);
    }
    public function editimage($id)
    {
        $data = imagedata::where('id',$id)->first();
        return view('editimage')->with('data',$data);
    }
    public function updateimgpost(Request $request , $id)
    {
        $data = Request::all();
        $upload = array();
        $upload['txt'] = $data['txt'];
        if(!empty(Request::file('video'))){
            $videodata = Request::file('video');
            $video = $videodata->getClientOriginalName();
            $destinationPath = base_path('/storage/app/public/Images/Uploads/videos');
            $videodata->move($destinationPath, $video);
            $fullpath_video = 'Images/Uploads/videos/'.$video;
            $upload['video'] = $fullpath_video;
        }
        if(!empty(Request::file('obj'))){
            $objdata = Request::file('obj');
            $obj = $objdata->getClientOriginalName();
            $destinationPath = base_path('/storage/app/public/Images/Uploads/3d_obj');
            $objdata->move($destinationPath, $obj);
            $fullpath_obj = 'Images/Uploads/3d_obj/'.$obj;
            $upload['obj'] = $fullpath_obj;
        }
        if(!empty(Request::file('ref_image'))){
            $refimg = Request::file('ref_image');
            $ref_image = $refimg->getClientOriginalName();
            $destinationPath = base_path('/storage/app/public/Images/Uploads/Other_Image');
            $refimg->move($destinationPath, $ref_image);
            $fullpath_ref_image = 'Images/Uploads/Other_Image/'.$ref_image;
            $upload['ref_img'] = $fullpath_ref_image;
        }
        
        imagedata::where('id',$id)->update($upload);
        Session::flash('success','record updated successfully !');
        return redirect('/listdata');
        # code...
    }

    public function deleteimage($id)
    {
        # code...
        // print_r($id);die;
        $imgdata = imagedata::where('id',$id)->first();
        $resp = $this->deleteTarget($imgdata['target_id']);
        // print_r($resp);die;
        if($resp == 'TargetStatusProcessing'){
            Session::flash('error','Target status processing . Please delete after a while .');
            return redirect('/listdata');
        }elseif($resp == 'UnknownTarget'){
            Session::flash('error','Target is not in vuforia server .');
            return redirect('/listdata');
        }
        else{
            if($imgdata['image'] != '' && $imgdata['image']!= 'NULL'){
                if(file_exists(base_path('/storage/app').'/public'.$imgdata['image'])){
                    unlink(base_path('/storage/app').'/public'.$imgdata['image']);
                }
            }
            if($imgdata['video'] != '' && $imgdata['video']!= 'NULL'){
                if(file_exists(base_path('/storage/app').'/public/'.$imgdata['video'])){
                    unlink(base_path('/storage/app').'/public/'.$imgdata['video']);
                }
            }
            if($imgdata['obj'] != '' && $imgdata['obj']!= 'NULL'){
                if(file_exists(base_path('/storage/app').'/public/'.$imgdata['obj'])){
                    unlink(base_path('/storage/app').'/public/'.$imgdata['obj']);
                }
            }
            if($imgdata['ref_img'] != '' && $imgdata['ref_img']!= 'NULL'){
                if(file_exists(base_path('/storage/app').'/public/'.$imgdata['ref_img'])){
                    unlink(base_path('/storage/app').'/public/'.$imgdata['ref_img']);
                }
            }
            imagedata::where('id',$id)->delete();
            Session::flash('success','record deleted successfully !');
            return redirect('/listdata');
        }
        
    }

    // api functions

    public function addTarget($imagePath,$imageName) {
        // echo "string";die;
        $this->imagePath = $imagePath;
        $this->imageName = $imageName;
        $ch = curl_init(BASE_URL . TARGETS_PATH);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $image = file_get_contents($imagePath.$imageName);
        $image_base64 = base64_encode($image);
        // return $image_base64;
        // Use date to create unique filenames on server
        $date = new DateTime();
        $dateTime = $date->getTimestamp();
        $file = pathinfo($this->imageName);
        $filename       = $file['filename'];
        $fileextension = $file['extension'];
        $post_data = array(
            'name' => $filename. "_" .$dateTime. "." .$fileextension,
            'width' => 32.0,
            'image' => $image_base64,
            'application_metadata' => $this->createMetadata(),
            'active_flag' => 1
        );
        $body = json_encode($post_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders('POST', TARGETS_PATH, JSON_CONTENT_TYPE, $body));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        if ($info['http_code'] !== 201) {
            print 'Failed to add target: ' . $response;
            return 'none';
        } else {
            $vuforiaTargetID = json_decode($response)->target_id;
            print 'Successfully added target: ' . $vuforiaTargetID . "\n";
            return $vuforiaTargetID;
        }
    }
    private function getHeaders($method, $path = TARGETS_PATH, $content_type = '', $body = '') {
        $headers = array();
        $date = new DateTime("now", new \DateTimeZone("GMT"));
        $dateString = $date->format("D, d M Y H:i:s") . " GMT";
        $md5 = md5($body, false);
        $string_to_sign = $method . "\n" . $md5 . "\n" . $content_type . "\n" . $dateString . "\n" . $path;
        $signature = $this->hexToBase64(hash_hmac("sha1", $string_to_sign, SECRET_KEY));
        // print_r($signature);die;
        $headers[] = 'Authorization: VWS ' . ACCESS_KEY . ':' . $signature;
        $headers[] = 'Content-Type: ' . $content_type;
        $headers[] = 'Date: ' . $dateString;
        // $headers[] = 'Date: Sat, 23 Feb 2019 09:50:28 GMT';
        return $headers;
    }
    private function hexToBase64($hex){
        $return = "";
        foreach(str_split($hex, 2) as $pair){
            $return .= chr(hexdec($pair));
        }
        return base64_encode($return);
    }
    private function createMetadata() {
        $metadata = array(
            'id' => 1,
            'image_url' => $this->imagePath.$this->imageName
        );
        return base64_encode(json_encode($metadata));
    }
    public function deleteTarget($vuforiaTargetID) {
        $path = TARGETS_PATH . "/" . $vuforiaTargetID;
        $ch = curl_init(BASE_URL . $path);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders('DELETE', $path));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        if ($info['http_code'] !== 200) {
            $response1 = '';
            $response1 =  json_decode($response);
            $response1 = $response1->result_code;
            // print_r($response1);die;
            return ($response1);
            // die('Failed to delete target: ' . $response . "\n");
        }
        return json_decode($response);
    }
}
