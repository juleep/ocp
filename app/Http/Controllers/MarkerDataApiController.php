<?php

namespace App\Http\Controllers;

use Request;
use App\imagedata;

class MarkerDataApiController extends Controller
{
    //
    public function marker_data(Request $request)
    {
    	# code...
    	$data = Request::all();
    	// print_r($data->target_id);die;
    	$target_id = $data['target_id'];
    	$getdata = imagedata::where('target_id',$target_id)->first();
    	$basepath = url('/storage');
    	// print_r($basepath);die;
    	if($getdata['image'] != 'NULL' && $getdata['image'] != '')
    		$getdata['image'] = $basepath.$getdata['image']; 
    	if($getdata['video'] != 'NULL' && $getdata['video'] != '')
    	$getdata['video'] = $basepath.'/'.$getdata['video']; 
    	if($getdata['obj'] != 'NULL' && $getdata['obj'] != '')
    	$getdata['obj'] = $basepath.'/'.$getdata['obj']; 
    	if($getdata['ref_img'] != 'NULL' && $getdata['ref_img'] != '')
    	$getdata['ref_img'] = $basepath.'/'.$getdata['ref_img']; 
    	return json_encode($getdata);
    }
}
