
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="login-page">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-10">
                            Upload
                        </div>
                        <div class="col-md-2">
                            <a href="{{url('/listdata')}}" class="btn btn-success align-right">View List</a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    @if(Session::has('error'))
                        <p class="alert alert-danger">{{ Session::get('error') }}</p>
                    @endif
                    @if(Session::has('success'))
                        <p class="alert alert-info">{{ Session::get('success') }}</p>
                    @endif
                    <form class="form-horizontal" method="POST" action="{{url('/uploadimgpost')}}" enctype="multipart/form-data" id="uploadimg">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('mainimg') ? ' has-error' : '' }}">
                            <label for="mainimg" class="col-md-4 control-label">Image</label>

                            <div class="col-md-8">
                                <input id="mainimg" type="file" class="form-control" name="mainimg" value="{{ old('mainimg') }}" >

                                @if ($errors->has('mainimg'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mainimg') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" id="sbmit">
                                    Upload
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footscript')

<script src="{{url('/js/jquery.validate.min.js')}}"></script>
<script> 
    // $(document).ready(function(){
    //     alert('hii');
    // })  
    $(document).on('click','#sbmit',function () {
    $('#uploadimg').validate({ // initialize the plugin
        // alert('hii');
        rules: {
            mainimg: {
                required: true
            },
            // email: {
            //     required: true,
            //     email: true
            // },
            // phone: {
            //     required: true
            // },
            // details: {
            //     required: true
            // }
        }
    });
});
</script>
@endsection