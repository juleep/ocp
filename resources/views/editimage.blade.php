
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-10">
                            Edit Image
                        </div>
                        <div class="col-md-2">
                            <a href="{{url('/listdata')}}" class="btn btn-success align-right">View List</a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{url('/updateimgpost')}}/{{$data['id']}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <h5><strong>Target Id :</strong> {{$data['target_id']}}</h5>
                        <hr>
                        <div class="form-group{{ $errors->has('mainimg') ? ' has-error' : '' }}">
                            <label for="mainimg" class="col-md-4 control-label">Image</label>
                            <div class="col-md-8 row">
                                <div class="col-md-3">
                                    <img src="{{asset('/storage')}}{{$data['image']}}" class="img-responsive">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <p>Other Fields</p>
                        <div class="form-group{{ $errors->has('txt') ? ' has-error' : '' }}">
                            <label for="txt" class="col-md-4 control-label">Text</label>
                            <div class="col-md-8 row">
                                <div class="col-md-10">
                                    <input id="txt" type="text" class="form-control" name="txt" value="{{ $data['txt'] }}" >
                                </div>
                                @if ($errors->has('txt'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txt') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('video') ? ' has-error' : '' }}">
                            <label for="video" class="col-md-4 control-label">video</label>
                            <div class="col-md-8 row">
                                @if(!empty($data['video']))
                                <div class="col-md-10">
                                    <input id="video" type="file" class="form-control" name="video" value="{{ old('video') }}" >
                                </div>
                                <div class="col-md-2">
                                    <a target="_blank" href="{{asset('storage')}}/{{$data['video']}}">See Video</a> 
                                </div>
                                @else
                                <div class="col-md-10">
                                    <input id="video" type="file" class="form-control" name="video" value="{{ old('video') }}" >
                                </div>
                                @endif
                                @if ($errors->has('video'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('video') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('obj') ? ' has-error' : '' }}">
                            <label for="obj" class="col-md-4 control-label">3D Object</label>

                            <div class="col-md-8 row">
                                @if(!empty($data['obj']))
                                    <div class="col-md-10">
                                        <input id="obj" type="file" class="form-control" name="obj" value="{{ old('obj') }}" >
                                    </div>
                                    <div class="col-md-2">
                                        <a target="_blank" href="{{asset('storage')}}/{{$data['obj']}}">See Object</a> 
                                    </div>
                                @else
                                    <div class="col-md-10">
                                        <input id="obj" type="file" class="form-control" name="obj" value="{{ old('obj') }}" >
                                    </div>
                                @endif
                                @if ($errors->has('obj'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('obj') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('ref_img') ? ' has-error' : '' }}">
                            <label for="ref_img" class="col-md-4 control-label">Other Image</label>
                            <div class="col-md-8 row">
                                @if(!empty($data['ref_img']))
                                <div class="col-md-10">
                                    <input id="ref_img" type="file" class="form-control" name="ref_image" value="{{ old('ref_img') }}" >
                                </div>
                                <div class="col-md-2">
                                    <a target="_blank" href="{{asset('storage')}}/{{$data['ref_img']}}">See Image</a>
                                </div>
                                @else
                                    <div class="col-md-10">
                                        <input id="ref_img" type="file" class="form-control" name="ref_image" value="{{ old('ref_img') }}" >
                                    </div>
                                @endif
                                @if ($errors->has('ref_img'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ref_image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Upload
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
