
@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-10">
                            List Images
                        </div>
                        <div class="col-md-2">
                            <a href="{{url('/uploadimg')}}" class="btn btn-success align-right">Upload New</a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    @if(Session::has('error'))
                        <p class="alert alert-danger">{{ Session::get('error') }}</p>
                    @endif
                    @if(Session::has('success'))
                        <p class="alert alert-info">{{ Session::get('success') }}</p>
                    @endif
                    <table id="example" class="table table-bordered ">
                        <thead>
                            <tr>
                            <th>Sr No.</th>
                            <th>Image</th>
                            <th>Target_id</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $key=>$dt)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td><img class="img-responsive" src="{{asset('storage')}}{{$dt['image']}}" ></td>
                                    <td>{{$dt['target_id']}}</td>
                                    <td><a href="{{URL::to('/')}}/editimage/{{$dt['id']}}" class="btn btn-success">Edit</a>&nbsp;&nbsp;<a href="{{url('/')}}/deleteimage/{{$dt['id']}}" class="btn btn-danger">delete</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footscript')
<script type="text/javascript" src="{{url('/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{url('/js/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection
