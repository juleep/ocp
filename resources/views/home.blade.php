@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!

                    <h3>Go to</h3>

                    <h4><a href="{{url('/listdata')}}"> List Uploaded Image</a></h4>
                    <h4><a href="{{url('/uploadimg')}}"> Upload Image</a></h4>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
